const Sequelize = require('sequelize');
const sequelize = require('../config/database');

  const Login = sequelize.define('login', {
    // define each field in login table
    username: {                         
      type: Sequelize.STRING,
      primaryKey: true
    },
    password: {
      type: Sequelize.STRING
    },
    login_time: {
        type: Sequelize.STRING
    },
    logout_time: {
        type: Sequelize.STRING
    },
    created_date: {
        type: Sequelize.STRING
    },
    updated_date: {
        type: Sequelize.STRING
    }
  },{
    freezeTableName: true,              
    timestamps: false
  }
  
  );

  module.exports = Login;
  