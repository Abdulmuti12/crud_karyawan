const Sequelize = require('sequelize');
const sequelize = require('../config/database');

  const Employee = sequelize.define('employee', { // employee table
    nik: {                         
      type: Sequelize.STRING,
      primaryKey: true
    },
    nama: {
      type: Sequelize.STRING
    },
    jenis_kelamin: {
        type: Sequelize.STRING
    },
    divisi: {
        type: Sequelize.STRING
    },
    department: {
        type: Sequelize.STRING
    },
    posisi: {
        type: Sequelize.STRING
    },
    cabang: {
        type: Sequelize.STRING
    }
  },{
    freezeTableName: true,              
    timestamps: false
  }
  
  );

  module.exports = Employee;
  