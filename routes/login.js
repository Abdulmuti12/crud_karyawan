// we call express library  
var express = require('express'); 

// define router as express.Router()
var router = express.Router();     

// Load md5, because i use md5 to encrypt the password
var md5 = require('md5');

// load Login Model in models folder
var LoginModel = require('../models/login');

// Load Constants variable
var constant = require('../config/constants');

// CSRF Protection
var csrf = require('csurf');
var csrfProtection = csrf({ cookie: true })


// -----------------------------------------------------//

// this mean... as route index
router.get('/', csrfProtection, function(req, res, next) {  
  res.render('login', {
    csrfToken: req.csrfToken()
  });
});

// see? i use .post in route, that mean
// this route handle the post method / request
router.post('/do_login', csrfProtection, async (req, res, next) => { 
 
  // to catch data in method post, we should use req.body
  var {username, password} = req.body;
  
  // this ORM query is using (sequelize) library, and equal to :
  // select * from login where username = ? and password = ? limit 1
  var query = await LoginModel.findOne({
    where: {
      username: username,
      password: md5(password)
    }
  })

  if(query) {
    // set session
    req.session.username = query.username

    // redirect to url home
    res.redirect('/home')
  }
  else {
    // set flash message
    req.flash('info', '<div class="alert alert-danger text-center"><strong>Username</strong> atau <strong>Password</strong> anda Salah!</div>');
    // redirect the url to login
    res.redirect('/login')  
  } 
})

// Route to handle log out
router.get('/logout', (req, res, next) => {
  if (req.session) {
    // delete session object
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }

})



module.exports = router;    
